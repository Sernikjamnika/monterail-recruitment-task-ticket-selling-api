# monterail-recruitment-task-ticket-selling-api

## Installation

This is Monterail recruitment task - an app to create Events and sell tickets.

To run this app you have to have Docker installed. I assume you have already done it. If not firstly got to this [page](https://docs.docker.com/install/).

When Docker is installed go to project root directory and type this to build the image.
```sh
$ docker-compose build
```

Next type this to run the image. All necessary migrations will be made and applied in this step.
```sh
$ docker-compose up
```

Migrations will be applied automatically.

Now the api is avaliable [here](http://localhost:8000). Easy isn't it :-)

During building proccess those modules are downloaded:
- Django==2.2.3
- djangorestframework==3.9.4
- psycopg2-binary==2.8.3
- django-filter==2.1.0
- drf-yasg==1.16.0
- celery==4.3.0
- django-celery-results==1.1.2


## Usage

For detailed info go to [redoc](http://localhost:8000/redoc/)

## Tests
To run tests for the project use this command. It will run all tests in the project.
```sh
$ docker exec -ti monterail-recruitment-task-ticket-selling-api_backend_1 python manage.py test
```
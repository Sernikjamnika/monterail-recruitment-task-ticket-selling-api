from celery import shared_task
from celery.task.control import revoke
from django.db import transaction

from ticket_selling_api.models import Booking


@shared_task(bind=True)
def reject_booking(self):
    with transaction.atomic():
        booking = Booking.objects.select_related('ticket')\
            .select_for_update(of=('ticket',))\
            .get(booking_id=self.request.id)
        booking.status = 'R'
        booking.ticket.reserved -= 1
        booking.ticket.save()
        booking.save()

from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    EventViewSet,
    CreateEventViewSet,
    TicketViewSet,
    BookingViewSet,
    BookingStatisticsView,
    PaymentView,
)


router = DefaultRouter()
router.register('event', EventViewSet, basename='event')
router.register('event_create', CreateEventViewSet, basename='event_create')
router.register('ticket', TicketViewSet, basename='ticket')
router.register('booking', BookingViewSet, basename='booking')

urlpatterns = [
    path('ticket_statistics/', BookingStatisticsView.as_view()),
    path('payment/', PaymentView.as_view())
]

urlpatterns += router.urls

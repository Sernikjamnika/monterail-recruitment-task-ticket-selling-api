from rest_framework import exceptions


class CardError(Exception):
   pass


class PaymentError(Exception):
   pass


class CurrencyError(Exception):
   pass

class NotEnoughAmountOfMoney(exceptions.APIException):
    status = 400
    default_detail = 'Not sufficient amount of money has been paid'

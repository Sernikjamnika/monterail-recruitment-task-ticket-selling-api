from django.apps import AppConfig


class TicketSellingApiConfig(AppConfig):
    name = 'ticket_selling_api'

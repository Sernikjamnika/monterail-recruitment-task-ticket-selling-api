from rest_framework import serializers
from django.db import transaction

from .models import Event, Ticket, Booking
from .tasks import reject_booking
from ticket_selling_api import consts


class TicketSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ticket
        fields = '__all__'
        read_only_fields = ('id', 'event', 'reserved', 'paid')


class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = '__all__'
        read_only_fields = ('ticket_types',)


class CreateEventSerializer(serializers.Serializer):
    event = EventSerializer()
    tickets = TicketSerializer(many=True)

    def create(self, validated_data):
        tickets_info = validated_data.pop('tickets')
        ticket_types = [ticket_info['ticket_type'] for ticket_info in tickets_info]
        event = Event.objects.create(**validated_data['event'], ticket_types=ticket_types)
        tickets = []
        for ticket_info in tickets_info:
            tickets.append(
                Ticket(
                    event=event,
                    **ticket_info
                )
            )
        tickets = Ticket.objects.bulk_create(tickets)
        return {'event': event, 'tickets': tickets}


class BookingSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        with transaction.atomic():
            ticket = Ticket.objects.select_for_update().get(pk=self.data['ticket'])
            if ticket.quantity - (ticket.reserved + ticket.paid) > 0:
                ticket.reserved += 1
                ticket.save()
            else:
                raise serializers.ValidationError("There are no available tickets. Try booking later")

        task = reject_booking.apply_async(countdown=consts.BOOKING_REJECTION_TIME)
        return Booking.objects.create(**validated_data, booking_id=task.id)

    def validate_ticket(self, value):
        available_tickets = value.quantity - (value.reserved + value.paid)
        if available_tickets <= 0:
            raise serializers.ValidationError("There are no available tickets. Try booking later")
        return value

    class Meta:
        model = Booking
        fields = '__all__'
        read_only_fields = ('booking_id', 'status')

class BookingStatisticsSerializer(serializers.Serializer):
    name = serializers.CharField()
    reserved_tickets=serializers.IntegerField()
    paid_tickets=serializers.IntegerField()
    all_tickets=serializers.IntegerField()
    money_earned=serializers.FloatField()

class PaymentSerializer(serializers.Serializer):
    token = serializers.CharField()
    amount = serializers.FloatField()
    booking_id = serializers.CharField()

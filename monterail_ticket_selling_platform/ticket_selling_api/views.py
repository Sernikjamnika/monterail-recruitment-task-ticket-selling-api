from celery.task.control import revoke
from django_filters.rest_framework import DjangoFilterBackend
from django.db import transaction
from django.db.models import Count, F, Sum
from django.db.models.functions import Coalesce
from django.shortcuts import render
from drf_yasg.utils import swagger_auto_schema
from rest_framework import (generics, mixins, response, serializers, status,
                            views, viewsets)

import logging

from .exceptions import NotEnoughAmountOfMoney
from .filters import BookingStatisticsFilterSet
from .gateways import PaymentGateway
from .models import Booking, Event, Ticket
from .serializers import (BookingSerializer, BookingStatisticsSerializer,
                          CreateEventSerializer, EventSerializer,
                          PaymentSerializer, TicketSerializer)


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class CreateEventViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = CreateEventSerializer

class TicketViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['event']

class BookingViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        booking_instance = self.perform_create(serializer)
        serializer = self.serializer_class(booking_instance)
        headers = self.get_success_headers(serializer.data)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

class BookingStatisticsView(generics.ListAPIView):
    queryset = Event.objects
    filter_backends = [DjangoFilterBackend]
    filterset_class = BookingStatisticsFilterSet
    serializer_class = BookingStatisticsSerializer

    def list(self, request, *args, **kwargs):
        # queryset = self.filter_queryset(self.get_queryset())
        
        obj = Event.objects.annotate(
            reserved_tickets=Sum('ticket__reserved'),
            paid_tickets=Sum('ticket__paid'),
            all_tickets=Sum('ticket__quantity'),
            money_earned=Sum(F('ticket__paid') * F('ticket__price'))
        )

        serializer = self.get_serializer(obj, many=True)
        return response.Response(serializer.data)

class PaymentView(views.APIView):

    serializer_class = PaymentSerializer
    payment_gateway_class = PaymentGateway

    @swagger_auto_schema(request_body=PaymentSerializer)
    def post(self, request):
        serializer = self.serializer_class(request.data)
        booking_id = serializer.data.pop('booking_id')
        gateway = self.payment_gateway_class()
        payment_result = gateway.charge(serializer.data['amount'], serializer.data['token'])

        with transaction.atomic():
            booking = Booking.objects.select_for_update().select_related().get(booking_id=booking_id)
            if booking.ticket.price == payment_result.amount:
                booking.status = 'PD'
                booking.ticket.paid += 1
                booking.ticket.reserved -= 1
                booking.save()
                booking.ticket.save()
                revoke(booking_id)
            else:
                raise NotEnoughAmountOfMoney()
        return response.Response(serializer.data)

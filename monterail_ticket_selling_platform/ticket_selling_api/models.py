from django.contrib.postgres.fields import ArrayField
from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    begining_date = models.DateTimeField()
    ticket_types = ArrayField(
        base_field = models.CharField(max_length=30),
        size=15
    )

    def __str__(self):
        return self.name

class Ticket(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    ticket_type = models.CharField(max_length=30)
    quantity = models.PositiveIntegerField()
    reserved = models.PositiveIntegerField(default=0)
    paid = models.PositiveIntegerField(default=0)
    price = models.PositiveIntegerField()

    def __str__(self):
        return ' '.join([self.event.name, self.ticket_type])

class Booking(models.Model):
    BOOKING_STATUSES = (
        ('P', 'Pending'),
        ('R', 'Rejected'),
        ('PD','Paid')
    )
    booking_id = models.CharField(primary_key=True, max_length=50)
    booker = models.CharField(max_length=30)
    status = models.CharField(choices=BOOKING_STATUSES, default='P', max_length=5)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)

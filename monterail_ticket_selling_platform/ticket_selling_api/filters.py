from django_filters import rest_framework as filters

from .models import Event


class BookingStatisticsFilterSet(filters.FilterSet):
    ticket_type = filters.CharFilter(method='filter_ticket_type')

    def filter_ticket_type(self, queryset, name, value):
        return queryset.filter(ticket__ticket_type=value)

    class Meta:
        model = Event
        fields = ['ticket_type', 'name']

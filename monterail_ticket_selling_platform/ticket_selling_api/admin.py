from django.contrib import admin

from .models import Event, Ticket, Booking


admin.site.register([Event, Ticket, Booking])
